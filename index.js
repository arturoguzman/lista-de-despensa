const express = require('express');
const app = express();
const port = 5000;
const path = require('path');
const mongoose = require ('mongoose');
const mongodbatlas = require('./mongodb')

mongoose.connect(mongodbatlas.mdblogin, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
    useFindAndModify: false
});

app.set('view engine', 'ejs');
app.use(express.static(__dirname + '/public'));
app.use(express.urlencoded({extended:false}));

app.get('/', async (req,res)=> {
    const cosas = await cosasModel.find().sort({ createdAt: 'desc' })
    res.render('index', { cosas: cosas })
})

const cosasSchema = new mongoose.Schema({
    que: {
    type: String,
     required: true
    },
    cuanto: {
        type: String,
    required: true,
    },
    createdAt: {
        type: Date,
        default: Date.now
    }
})

const cosasModel = mongoose.model('cosasModel',cosasSchema);

app.get('/', (req, res) => {
    res.render('/', { cosas: new cosasModel() })
  })

app.post ('/', async (req, res) => {
    let cosas = new cosasModel({
        que: req.body.que,
        cuanto: req.body.cuanto
    })
    try {
    cosas = await cosas.save()
    res.redirect('back');
    } catch (err) {
        console.log(err)
        res.render('/')
    }
})


app.route("/remove/:id").get((req, res) => {
    const id = req.params.id;
    cosasModel.findByIdAndRemove(id, err => {
        if (err) return res.send(500, err);
        res.redirect("/");
    });
});

//console log connection successful

app.listen(port, () => console.log (`server started on ${port} :)`));

mongoose.connection.on('connected', () => {
    console.log('mongodb is connected :)')
});
